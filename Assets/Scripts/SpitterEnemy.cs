﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpitterEnemy : MonoBehaviour
{
    public float spitForce;
    public float detectRadius;
    public GameObject spitKindaThingIdk;
    public Transform spitSpawn;
    public Transform target;
    [Range(0, 10)]
    public float spitCooldown;

    private float timer;
    private Vector2 targetPos;
    private float shootAngle;

    private void Start()
    {
        target = GameObject.FindWithTag("Player").transform;
    }

    private void FixedUpdate()
    {
        CheckDistanceToPlayer();
       // print(Vector2.Distance(target.transform.position, gameObject.transform.position));
    }

    private void CheckDistanceToPlayer()
    {
        timer += Time.deltaTime;
        if (Vector2.Distance(target.transform.position, gameObject.transform.position)
                                               <= detectRadius)
        {
            if(timer >= spitCooldown)
            {
                Spit();
                print("pitú");
                timer = 0f;
            }           
            
        }

    }

    void Spit()
    {
        if (target)
        {
            targetPos = target.position - spitSpawn.position;
            shootAngle = Mathf.Atan2(targetPos.y, targetPos.x) * Mathf.Rad2Deg;
            spitSpawn.rotation = Quaternion.Euler(0f, 0f, shootAngle);
            GameObject spit = Instantiate(spitKindaThingIdk, spitSpawn.position, spitSpawn.rotation);
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), spit.GetComponent<Collider2D>());
            spit.GetComponent<Rigidbody2D>().velocity = spitSpawn.right * spitForce;
        }
        
        
       // Vector3 change = Vector2.MoveTowards(spit.transform.position, pos, spitForce).normalized;
       // spit.GetComponent<Rigidbody2D>().MovePosition(spit.transform.position + change * Time.deltaTime);
     
    }

    IEnumerator shootCooldown()
    {
        yield return new WaitForSeconds(2f);
    }
}
