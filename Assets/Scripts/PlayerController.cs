﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [HideInInspector]
    public Rigidbody2D rb;

    private CircleCollider2D circleCollider;

    [SerializeField]
    private LayerMask platformLayers;
    public float jumpForce;   
    Vector2 change;
    Animator animator;
    public bool jumping;
    public float speed;
    public float fallMultiplier = 2.5f;

    private bool jumpPressed = false;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        circleCollider = GetComponent<CircleCollider2D>();
    }

    private void Update()
    {
        change.x = Input.GetAxisRaw("Horizontal");
        //print(change);

        if (Input.GetButtonDown("Jump"))
            jumpPressed = true;
               
    }

    private void FixedUpdate()
    {
        CheckJump();
        ApplyGravity();
        MovePlayer();
    }

    private void CheckJump()
    {      
            if (isGrounded() && jumpPressed)
            {
                animator.SetBool("jumping", true);
                print("jumping");
                rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);                
                jumping = true;

                jumpPressed = false;
            }       

        if (!isGrounded())
        {
            jumping = true;
            animator.SetBool("jumping", true);
        }
        else
        {
            jumping = false;
            animator.SetBool("jumping", false);
        }
    }

    private void MovePlayer()
    {
        if (change != Vector2.zero)
        {
            //rb.MovePosition(rb.position + change * speed * Time.deltaTime);
            rb.velocity = new Vector2(speed * change.x, rb.velocity.y);
            animator.SetFloat("changeX", change.x);
            animator.SetBool("moving", true);
            if (change.x < 0)
                GetComponent<SpriteRenderer>().flipX = true;
            else
                GetComponent<SpriteRenderer>().flipX = false;
        }
        else
        {
            animator.SetBool("moving", false);
        }        
    }

    void ApplyGravity()
    {
        if (rb.velocity.y < 0)
            rb.gravityScale = fallMultiplier;
        else if (rb.velocity.y > 0 && !Input.GetButton("Jump"))
            rb.gravityScale = fallMultiplier;
        else
            rb.gravityScale = 1f;
    }

    private bool isGrounded()
    {
        RaycastHit2D raycast = Physics2D.CircleCast(circleCollider.bounds.center, circleCollider.radius, Vector2.down, .04f, platformLayers);
        return raycast.collider != null;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Enemy"))
        {
            Destroy(gameObject);
        }
    }

}
