﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D other)
    {
        
        if (other.CompareTag("Player"))
        {
            PlayerController player = other.GetComponent<PlayerController>();
            player.rb.velocity = new Vector2(player.rb.velocity.x, player.jumpForce);
            Destroy(gameObject);
        }
            
    }

}
