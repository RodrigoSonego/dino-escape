﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningEnemy : MonoBehaviour
{
    public float speed;
    public float jumpForce;
    Rigidbody2D rb;
    public Transform detector;
    bool jumping;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(-speed, rb.velocity.y);
        CheckForPit();
    }

    void CheckForPit()
    {

        RaycastHit2D ray = Physics2D.Raycast((Vector2)detector.position, Vector2.down * 5);
        if(ray.collider == null)
        {
            if (!jumping)
            {
                Jump();
                jumping = true;
            }
              
        } else
        {
            jumping = false;
        }

    }

    void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, jumpForce);
    }
}
